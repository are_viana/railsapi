class CreateOrderLines < ActiveRecord::Migration[5.2]
  def change
    create_table :order_lines do |t|
      t.references :order, foreign_key: true, on_delete: :cascade
      t.references :product, foreign_key: true
      t.decimal :price, precision: 8, scale: 2, default: 0
      t.integer :quantity, default: 0
      t.decimal :total, precision: 10, scale: 2, default: 0

      t.timestamps
    end
  end
end
