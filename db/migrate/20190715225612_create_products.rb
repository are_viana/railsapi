class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :sku
      t.string :name
      t.string :description
      t.decimal :price, precision: 8, scale: 2, default: 0
      t.integer :stock, default: 0
      t.integer :status

      t.timestamps
    end
    add_index :products, :sku, unique: true
  end
end
