class CreateProductLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :product_logs do |t|
      t.references :admin, index: true, foreign_key: { to_table: :users }
      t.references :product, foreing_key: true
      t.string :name_before
      t.string :name_after
      t.string :description_before
      t.string :description_after
      t.decimal :price_before, precision: 8, scale: 2
      t.decimal :price_after, precision: 8, scale: 2
      t.timestamps
    end
  end
end
