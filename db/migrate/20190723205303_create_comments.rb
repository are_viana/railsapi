class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.text :description
      t.references :commentable, polymorphic: true
      t.references :user, foreign_key: true
      t.decimal :rati, precision: 5, scale: 2
      t.integer :status

      t.timestamps
    end
  end
end
