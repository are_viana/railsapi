class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.references :user, foreign_key: true, optional: true
      t.date :finished_at
      t.decimal :total, precision: 10, scale: 2, default: 0

      t.timestamps
    end
    add_column :orders, :status, :order_status
    add_index :orders, :status
  end
end
