class AddCachedVotesToProducts < ActiveRecord::Migration[5.2]
  def change
    change_table :products do |t|
      t.integer :cached_votes_up, default: 0
      t.integer :cached_votes_down, default: 0
    end

    # Uncomment this line to force caching of existing votes
    Product.find_each(&:update_cached_votes)
  end
end
