class AddDiscardToProducts < ActiveRecord::Migration[5.2]
  def up
    add_column :products, :discarded_at, :datetime
    add_index :products, :discarded_at
  end
  def down
    remove_index :products, name: 'index_products_on_discarded_at'
    remove_column :products, :discarded_at
  end
end
