class CreateProductTags < ActiveRecord::Migration[5.2]
  def change
    create_table :product_tags, id: false do |t|
      t.references :product, foreing_key: true, on_delete: :cascade
      t.references :tag, foreing_key: true, on_delete: :cascade

    end
    add_index :product_tags, [:product_id, :tag_id], unique: true
  end
end
