class EnumStatusOrder < ActiveRecord::Migration[5.2]
  def change
    execute %Q[
      CREATE TYPE order_status AS ENUM ('open','complete','abandoned','canceled');
      ]
  end
end
