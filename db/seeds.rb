# frozen_string_literal: true

# Test data

require 'securerandom'
require 'date'

User.create(
  email: 'admin@example.com',
  password: '123456',
  role: 0
)
User.create(
  email: 'support@example.com',
  password: '123456',
  role: 1
)
User.create(
  email: 'customer@example.com',
  password: '123456',
  role: 2
)
3.times do |i|
  i += 1
  Profile.create(
    user_id: i,
    address: Faker::Address.street_address,
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    phone: Faker::PhoneNumber.cell_phone
  )
end

pn = ['Doritos', 'Galletas', 'CocaCola', 'Dr.Peppe', 'Jugo del Valle',
      'Papitas', 'Cafe helado', 'Platanitos', 'Lechuga', 'Tomates',
      'Escoba', 'Trapeador', 'Aspiradora']
pp = [0.35, 0.7, 0.50, 0.55, 1.0, 0.30, 0.85, 0.15, 1.20, 1.15, 5.0, 5.20, 15.0]
tn = %w[Drink Candy Healthy Imported Local Unhealthy Food Houseware]

13.times do |i|
  Product.create(
    sku: SecureRandom.uuid,
    name: pn[i],
    description: "Description of #{pn[i]}",
    price: pp[i],
    stock: rand(1..25),
    status: Product.statuses.values.sample
  )
end

8.times do |i|
  Tag.create(name: tn[i])
end

Product.unscoped.find(3).tags << Tag.find(6)
Product.unscoped.find(3).tags << Tag.find(1)
Product.unscoped.find(4).tags << Tag.find(1)
Product.unscoped.find(5).tags << Tag.find(1)
Product.unscoped.find(7).tags << Tag.find(1)
Product.unscoped.find(9).tags << Tag.find(3)
Product.unscoped.find(10).tags << Tag.find(3)
Product.unscoped.find(11).tags << Tag.find(8)
Product.unscoped.find(12).tags << Tag.find(8)
Product.unscoped.find(13).tags << Tag.find(8)

10.times do
  Order.create(
    user_id: rand(1..2),
    finished_at: nil,
    total: 0,
    status: Order.statuses.values.sample
  )
end

ProductLog.create(
  admin_id: 1,
  product_id: 1,
  name_before: 'Old_name1',
  name_after: 'New_name1',
  description_before: 'Old_description1',
  description_after: 'New_description1',
  price_before: 0.25,
  price_after: 0.35
)
ProductLog.create(
  admin_id: 1,
  product_id: 2,
  name_before: 'Old_name2',
  name_after: 'New_name2',
  description_before: 'Old_description2',
  description_after: 'New_description2',
  price_before: 0.45,
  price_after: 0.65
)
ProductLog.create(
  admin_id: 2,
  product_id: 3,
  name_before: 'Old_name3',
  name_after: 'New_name3',
  description_before: 'Old_description3',
  description_after: 'New_description3',
  price_before: 0.35,
  price_after: 0.35
)

n = 1
Order.all.each do |o|
  5.times do
    cp = Product.unscoped.find(n)
    quantity = rand(1..cp.stock)
    o.order_lines.create(
      product_id: cp.id,
      price: cp.price,
      quantity: quantity,
      total: cp.price * quantity
    )
    # OrderLine > before_save :calculate_total, after_save :update_order(total)
    n >= 13 ? n = 1 : n += 1
  end
  # Order > before_save: update_total
  o.update_total
  o.save
end

# Add comments
Product.all.each do |p|
  p.comments.create(
    description: Faker::Lorem.sentence(5),
    user_id: 1,
    rati: rand(1..5)
  )
end

# Data created
puts 'The following records were created:'
puts "#{User.count} users"
puts "#{Profile.count} profiles"
puts "#{Product.unscoped.count} products"
puts "#{Order.count} order"
puts "#{OrderLine.count} order lines"
puts "#{Tag.count} tags"
puts "#{ProductTag.count} Tags-Product relations"
puts "#{Comment.count} Comments"
puts "#{ProductLog.count} Product Logs"

# To see more products in customer index, uncomment the following line:
# Product.update_all(status: 'active')
