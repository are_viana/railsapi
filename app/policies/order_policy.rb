# frozen_string_literal: true

# To Cart permissions
class OrderPolicy < ApplicationPolicy
  def show?
    if user.nil?
      if record.id.nil?
        true
      elsif record.id.present? && record.user_id.nil?
        true
      elsif record.user_id.present?
        false
      end
    elsif user.present?
      if record.user_id == user.id
        true
      elsif record.user_id != user.id
        false
      end
    else
      false
    end
  end
end
