# frozen_string_literal: true

# Have all permissions of this class
class ProductPolicy < ApplicationPolicy
  # Scope to Product#index
  class Scope < Scope
    def resolve
      if user.present? && user.admin?
        scope.kept
      else
        scope.active
      end
    end
  end

  def create?
    user.admin?
  end

  def update?
    user.admin? || user.support?
  end

  def destroy?
    user.admin?
  end

  def discarded?
    user.admin?
  end
end
# Product.all.order('discarded_at DESC, name'))