# frozen_string_literal: true

# Have only one scope to ProductLog
class ProductLogPolicy < ApplicationPolicy
  # Scope to ProductPriceLogs#index
  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      else
        raise Pundit::NotAuthorizedError
      end
    end
  end
end
