# frozen_string_literal: true

# Policies of actions in comments controller
class CommentPolicy < ApplicationPolicy
  def update?
    !user.customer?
  end

  def destroy?
    user.admin?
  end
end
