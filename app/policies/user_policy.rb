# frozen_string_literal: true

# Have all permissions of this class
class UserPolicy < ApplicationPolicy
  # Scope to Registration#index
  class Scope < Scope
    def resolve
      if user.present? && (user.admin? || user.support?)
        scope.kept
      else
        scope.customer
      end
    end
  end

  def new?
    user.admin? || user.support?
  end

  def create?
    user.admin? || user.support?
  end

  def destroy?
    user.admin? && !record.admin?
  end
end