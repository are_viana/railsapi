# frozen_string_literal: true

# Module to show the results of actions of the service objects
module Result
  def result(model, operation)
    {
      model: model,
      msj: "#{model.class.name.demodulize} has been successfully #{operation}",
      saved: model.save!
    }
  end
end
