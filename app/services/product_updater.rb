# frozen_string_literal: true

# Service Object to update a product and
# register its logs if is an admin or support
class ProductUpdater
  include Callable

  attr_reader :product, :product_id, :user, :name, :description, :price
  attr_accessor :params

  def initialize(params, product_id, user)
    @user = user
    @product_id = product_id
    @params = params
  end

  def call
    find_old_data
    #self.params = params.except(:price) if user.support?
    save_logs(name, description, price) if product.update!(params)
    product
  end

  def find_old_data
    @product = Product.find(product_id)
    @name = product.name
    @description = product.description
    @price = product.price
  end

  private

  def save_logs(old_name, old_description, old_price)
    ProductLog.create(
      admin_id: user.id,
      product_id: product.id,
      name_before: old_name,
      name_after: product.name,
      description_before: old_description,
      description_after: product.description,
      price_before: old_price,
      price_after: product.price
    )
  end
end
