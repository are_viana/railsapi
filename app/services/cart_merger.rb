# frozen_string_literal: true

# Service Object to merge carts
class CartMerger
  include Callable
  attr_reader :user
  attr_accessor :order

  def initialize(order, user)
    @order = order
    @user = user
  end

  def call
    if !user.orders.blank? && user.orders.last.open?
      order_now = user.orders.last
      lines_merge(order, order_now) if order.present?# merge lines quant >= 1
      order = order_now
    else # si no hay abierta, dejarlo como orden nueva
      self.order.user_id = user.id
    end
    self.order.save!
    order
  end

  def lines_merge(order1, order2)
    order1.products.each do |p|
      if order2.order_lines.exists?(product_id: p.id)
        order_line = order2.order_lines.find_by(product_id: p.id)
        order_line.quantity += p.order_lines.select(:quantity).first.to_i
        order_line.save!
      else
        order2.order_lines << p.order_lines.first
      end
      order1.destroy #xq ya sae lo pasé todo a la q estaba abiert
    end
  end
end
