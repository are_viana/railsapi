# frozen_string_literal: true

# Service Object to search a product
class ProductSearcher
  include Callable
  attr_reader :params
  attr_accessor :products

  def initialize(model, params)
    @products = model
    @params = params
  end

  def call
    if params.present?
      name = params[:name]
      tags_ids = params[:tag_ids]
      popularity = params[:popu]
      self.products = ProductsQuery.new(products).like_name(name) if name.present?
      self.products = ProductsQuery.new(products).with_tags(tags_ids) if tags_ids.present?
      self.products = ProductsQuery.new(products).popularity if popularity.present?
    end
    products
  end
end
