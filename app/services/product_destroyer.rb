# frozen_string_literal: true

# Service Object to destroy a product
class ProductDestroyer
  include Callable
  attr_reader :product

  def initialize(id)
    @product = Product.kept.find(id.to_i)
  end

  def call
    product.inactive! if product.discard
    product
  end
end
