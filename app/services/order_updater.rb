# frozen_string_literal: true

# Service Object to update quantities of current order
class OrderUpdater
  include Callable
  attr_reader :params
  attr_accessor :order
  def initialize(order, params)
    @order = order
    @params = params
  end

  def call
    order_lines = order.order_lines
    update_quantities(order_lines)
    order.update_total!
    order
  end

  private

  def update_quantities(lines)
    lines.each_with_index do |ol, i|
      ol.update_attribute(:quantity, params["q_#{i}"])
      ol.save!
    end
  end
end
