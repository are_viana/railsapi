# frozen_string_literal: true

# Service Object to create a product with its tags
class ProductCreator
  include Callable
  include Image
  attr_reader :tags, :params, :image
  attr_accessor :product

  def initialize(params)
    @tags = params[:tag_ids]
    @image = params[:image]
    @params = params.except(:tag_ids, :image)
  end

  def call
    self.product = Product.new(params)
    if product.save!
      include_tags if tags
      include_image if image && image[:img]
    end
    product
  end

  private

  def include_image
    product.image.attach(image[:img])
    resize(product.image_on_disk, image[:height], image[:width])
  end

  def include_tags
    tags.each { |id| product.tags << Tag.find(id.to_i) }
  end
end
