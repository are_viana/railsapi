# frozen_string_literal: true

# Service Object to return current cart
class CartFinder
  include Callable
  attr_reader :user
  attr_accessor :order

  def initialize(user)
    @user = user
  end

  def call
    if user.present?
      if user.orders.present? && user.orders.last.open?
        order = user.orders.last
      else
        order = user.orders.new(status: 'open')
      end
    else
      if Order.last.open?
        order = Order.last
      else
        order = Order.new(status: 'open')
      end
    end
    order
  end
end
