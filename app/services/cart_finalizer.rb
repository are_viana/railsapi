# frozen_string_literal: true

# Service Object to finalize order
class CartFinalizer
  include Callable
  attr_accessor :order
  def initialize(order)
    @order = order
  end

  def call
    if order.complete!
      order.update_attribute(:finished_at, Date.today)
      order.order_lines.each(&:update_product_stock)
      order.products.each(&:change_status)
    end
    order
  end
end
