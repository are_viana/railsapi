# frozen_string_literal: true

# Service Object to create order_lines in the current order
class CartCreator
  include Callable
  attr_reader :product_params
  attr_accessor :order

  def initialize(order, product_params)
    @product_params = product_params
    @order = order
  end

  def call
    product = Product.find(product_params[:product_id])
    order_line = check_repeated(order, product)
    if order_line.save! # First update line_total
      order.save! # Then update order_total
    end
    order
  end

  private

  def quantity
    if product_params[:quantity].present?
      product_params[:quantity].to_i
    else
      1
    end
  end

  def check_repeated(model, product)
    if model.order_lines.exists?(product_id: product.id)
      order_line = model.order_lines.find_by(product_id: product.id)
      order_line.quantity += quantity
    else
      order_line = model.order_lines.new(lines_params(product))
    end
    order_line
  end

  def lines_params(product)
    {
      product_id: product.id,
      price: product.price,
      quantity: quantity
    }
  end
end
