# frozen_string_literal: true

# Form object for users with their profiles
class RegistrationForm
  include ActiveModel::Model
  attr_accessor :email, :password, :user
  attr_accessor :role
  attr_accessor :address, :first_name, :last_name, :phone
  validates :first_name, presence: true
  validates :email, presence: true
  validates :password, presence: true
  validates :role, presence: true, if: -> { user.admin? }
  # validates_confirmation_of :password

  def save
    return false unless valid?

    self.role = 'customer' if user.support?
    new_user = User.create(email: email, password: password, role: role)
    create_profile(new_user) if new_user.save
  end

  def create_profile(model)
    model.create_profile(
      address: address,
      first_name: first_name,
      last_name: last_name,
      phone: phone
    )
  end
end
