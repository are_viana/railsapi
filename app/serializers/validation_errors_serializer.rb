# https://blog.rebased.pl/2016/11/07/api-error-handling.html
class ValidationErrorsSerializer
  attr_reader :record

  def initialize(record)
    @record = record.record
  end

  def serialize
    record.errors.messages.map do |field, details|
      details.map do |error_details|
        ValidationErrorSerializer.new(record, field, error_details).serialize
      end
    end.flatten
  end
end