# frozen_string_literal: true

# Class for ApplicationMailer
class ApplicationMailer < ActionMailer::Base
  default from: 'notifications@snackstore.com'
  layout 'mailer'
end
