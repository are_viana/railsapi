# frozen_string_literal: true

# Custom emails to send
class UserMailer < ApplicationMailer
  def stock_email(user, product)
    @user = user
    @product = product
    mail(
      to: @user.email,
      subject: 'Test API'
    ) do |format|
      format.text
    end
  end
end
