# frozen_string_literal: true

# Class for product price log
class ProductLog < ApplicationRecord
  belongs_to :product
  belongs_to :user, foreign_key: 'admin_id'

  validates :admin_id, :product_id, presence: true
  validates :price_before, :price_after, presence: true
  validates :price_before, :price_after, numericality: { greater_than_or_equal_to: 0.0 }

  delegate :email, to: :user, prefix: true
end
