# frozen_string_literal: true

# Class for product
class Product < ApplicationRecord
  include Discard::Model
  paginates_per 6
  acts_as_votable
  has_one_attached :image

  enum status: { active: 1, inactive: 0 }
  has_many :order_lines
  has_many :orders, through: :order_lines
  has_many :product_tags, dependent: :destroy
  has_many :tags, through: :product_tags
  has_many :product_logs
  has_many :comments, as: :commentable

  validates :sku, :name, uniqueness: true
  validates :name, :status, presence: true
  validates :price, numericality: { greater_than_or_equal_to: 0.0 }
  validates :stock, presence: true, numericality: { only_integer: true }
  validates :stock, inclusion: { in: 0..25, message: 'The value %{value} is not between in 0 and 25' }

  before_create :generate_sku

  def image_on_disk
    ActiveStorage::Blob.service.send(:path_for, image.key) if image.attached?
  end

  def generate_sku
    self.sku = SecureRandom.uuid
  end

  def avarage_rating
    (comments.sum(:rati) / comments.select(:rati).where('rati > 0').count).round(2)
  end

  def poor_stock?
    stock <= 3
  end

  def change_status
    inactive! if stock.zero?
  end

  scope :name_asc, -> { order(name: :asc) }

  # Callbacks of soft delete with Discard
  after_discard do
    comments.discard_all
  end

  after_undiscard do
    comments.undiscard_all
  end
end
