# frozen_string_literal: true

# Class for product tag
class ProductTag < ApplicationRecord
  belongs_to :product
  belongs_to :tag
  validates :tag_id, presence: true
  validates :product_id, uniqueness: { scope: :tag_id }, presence: true
end
