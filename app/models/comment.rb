# frozen_string_literal: true

# Class for comments
class Comment < ApplicationRecord
  include Discard::Model
  belongs_to :commentable, polymorphic: true
  belongs_to :user

  validates :user_id, presence: true
  validates :description, presence: true
  validates :rati, allow_nil: true, numericality: true
  validates :rati, inclusion: { in: 0..5, message: 'The value %{value} is not between in 0 and 5' }
  validates :commentable_type, presence: true
  validates :commentable_id, presence: true, numericality: { only_integer: true }
  enum status: { approved: 1, pending: 0, rejected: 2 }

  delegate :full_name, :email, to: :user, prefix: true # Belongs to User
end
