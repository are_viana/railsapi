# frozen_string_literal: true

# Class for orders
class Order < ApplicationRecord
  include Discard::Model

  belongs_to :user, optional: true
  has_many :order_lines, dependent: :destroy
  has_many :products, through: :order_lines
  has_many :comments, as: :commentable

  enum status: {
    open: 'open',
    complete: 'complete',
    abandoned: 'abandoned',
    canceled: 'canceled'
  }

  validates :status, presence: true
  validates :total, numericality: { greater_than_or_equal_to: 0.0 }

  before_create :init_order
  before_save :update_total

  def init_order
    self.status = 'open'
  end

  def update_total
    self.total = order_lines.sum(:total)
  end

  def repeated_lines
    order_lines.exists?(product_id: product.id)
  end

  # Callbacks of soft delete with Discard
  # after_discard do
  #   comments.discard_all
  # end

  # after_undiscard do
  #   comments.undiscard_all
  # end
end
