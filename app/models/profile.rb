# frozen_string_literal: true

# Class for profile
class Profile < ApplicationRecord
  belongs_to :user
  validates :user_id, presence: true, numericality: { only_integer: true }
  validates :first_name, :last_name, presence: true

  def full_name
    first_name + ' ' + last_name
  end
end
