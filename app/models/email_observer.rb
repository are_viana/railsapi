class EmailObserver < ActiveRecord::Observer
  observe :product
  def after_update(product)
    if product.previous_changes[:stock]
      EmailWorker.perform_async(product.id) if product.poor_stock? && product.get_likes.size.positive?
    end
  end
end
