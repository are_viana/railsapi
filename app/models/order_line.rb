# frozen_string_literal: true

# Class for order lines
class OrderLine < ApplicationRecord
  belongs_to :order # , touch: true
  belongs_to :product

  #validates :order_id, presence: true, unless: -> { order_id.new_record? }
  #validates :order_id, numericality: { only_integer: true }
  validates :product_id, numericality: { only_integer: true }, presence: true
  validates :quantity, numericality: { only_integer: true }
  validates :quantity, numericality: { greater_than_or_equal_to: 0 }
  validates :price, numericality: { greater_than_or_equal_to: 0.0 }

  before_save :verify_stock
  before_save :verify_price

  before_save :calculate_total

  after_destroy :update_total_order

  delegate :name, to: :product, prefix: true

  # I make sure there are no 2 lines with the same product in the same order
  #validate :validate_lines

  def validate_lines
    if new_record? && product.present? &&
       order.order_lines.exists?(product_id: product.id)
      errors.add(:product_id, "#{product.name} already exists in your cart")
    end
  end

  def verify_stock
    if quantity &&
       quantity > product.stock
      errors.add(:quantity, 'The stock limit has been exceded')
    end
  end

  # If the price change by season, and update line, raise error
  def verify_price
    errors.add(:price, 'Price does not match') unless price == product.price
  end

  # This method is used in a callback, it does not have an implicit self.total
  # if the mehod name is total.
  def calculate_total
    self.total = price * quantity
  end

  # Callback when the order_line is destroyed
  def update_total_order
    order.total -= total
    order.save
  end

  # ---------------------------AUXILIARY METHODS--------------------------------
  def calculate_order_total
    order.total += total
  end

  # If the product.price changes by season, it will recalculate the price,
  # so leave it as an auxiliary function
  def find_price
    self.price = product.price
  end

  def update_product_stock
    product.stock -= quantity
    product.save
  end
end
