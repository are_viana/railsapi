# frozen_string_literal: true

# Class principal model
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
