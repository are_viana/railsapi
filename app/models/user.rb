# frozen_string_literal: true

# Class for users
class User < ApplicationRecord
  include Discard::Model
  has_one :profile, dependent: :destroy
  has_many :product_logs
  has_many :orders
  has_many :comments, as: :commentable

  enum role: { admin: 0, support: 1, customer: 2 }
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  delegate :full_name, :address, :phone, to: :profile

  # Override Devise's method to be unable the login and stop the session
  # of discarded users
  def active_for_authentication?
    super && !discarded?
  end

  # To integrate Doorkeeper with Devise 
  # we need a method to find user by email and password.
  class << self
    def authenticate(email, password)
      user = User.find_for_authentication(email: email)
      user.try(:valid_password?, password) ? user : nil
    end
  end
end
