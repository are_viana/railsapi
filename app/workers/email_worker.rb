class EmailWorker
  include Sidekiq::Worker

  sidekiq_options retry: false

  def perform(product_id)
    @product = Product.find(product_id)
    @product.votes_for.includes(:voter).each do |vote|
      UserMailer.stock_email(vote.voter, @product).deliver
    end
    puts "The product #{@product.name} is soon to run out of stock,
     #{@product.cached_votes_up} emails has been generated"
  end
end
