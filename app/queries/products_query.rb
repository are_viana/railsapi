# frozen_string_literal: true

# Search consult of product
class ProductsQuery
  attr_reader :relation

  def initialize(relation)
    @relation = relation
  end

  def like_name(name)
    relation.where('products.name ILIKE ?', "%#{name}%")
  end

  def with_tags(ids)
    relation.joins(:tags)
            .where(tags: { id: ids }).distinct
  end

  def popularity
    relation.order('cached_votes_up DESC')
  end
end
