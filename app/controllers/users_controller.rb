# frozen_string_literal: true

# Controller to show a user with his profile
class UsersController < ApplicationController
  include Comments
  def index
    @users = policy_scope(User)
  end

  def show
    @users = policy_scope(User)
    @form = @users.find(params[:id])
    display_comments_user(@form)
  end
end
