class ProductsController < ApplicationController
  include Comments
  include MetaData
  require 'pp'
  before_action :found_product, only: %i[like dislike]

  def index
    @products = policy_scope(Product).page params[:page] # Policy_show products
    meta = meta_data(@products)
    @products = ProductSearcher.call(@products, search_params).page params[:page]
    pp(@products)
  end

  def show
    @products = policy_scope(Product)
    @product = Product.find(params[:id])
    display_comments(@product)
  end

  def like
    @product.liked_by current_user
    render 'votable'
  end

  def dislike
    @product.disliked_by current_user
    render 'votable'
  end

  private

  def found_product
    @product = Product.find(params[:product_id])
  end

  def search_params
    params.fetch(:search, {}).permit(:name, :popu, tag_ids: [])
  end
end
