# frozen_string_literal: true

class Admin::ProductLogsController < ApplicationController
  before_action :authenticate_user!

  def index
    @product_logs = Admin::ProductLogDecorator.decorate_collection(policy_scope(ProductLog))
  end
end
