# frozen_string_literal: true

# class for CRUD product in Admin
class Admin::ProductsController < ApplicationController
  include Errors
  before_action :authenticate_user!
  before_action :load_product, only: %i[show edit]
  before_action :load_tags, only: %i[new create]

  def new
    authorize Product
    @product = Product.new
  end

  def create
    authorize Product
    @product = ProductCreator.call(product_params)
    raise_exception(@product) # Break exception
    flash[:success] = 'Product has been successfully created'
    redirect_to product_path(@product.id)
  end

  def update
    authorize Product
    @product = ProductUpdater.call(product_params, params[:id], current_user)
    raise_exception(@product)
    flash[:success] = 'Product has been successfully updated'
    redirect_to product_path(@product.id)
  end

  def destroy
    authorize Product
    @product = ProductDestroyer.call(params[:id])
    raise_exception(@product)
    flash[:success] = 'Product was succesfully removed'
    redirect_to root_path
  end

  def list
    @products = Product.discarded
  end

  private

  def product_params
    params.require(:product).permit(:name, :description, :price, :stock, :status, tag_ids: [])
  end

  def load_product
    @product = Product.find(params[:id])
    @tags = @product.tags
  end

  def load_tags
    @tags = Tag.all
  end
end
