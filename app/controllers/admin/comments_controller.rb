# frozen_string_literal: true

# To destroy comment and index
class Admin::CommentsController < ApplicationController
  def index
    @comments = Comment.kept.where(commentable_type: 'User').pending
  end

  def update
    authorize Comment
    @comment = Comment.find(params[:id])
    @comment.status = 'approved'
    if @comment.save
      flash[:success] = 'Comment has been approved!'
    else
      raise_exception(@comment)
    end
    redirect_to user_path(@comment.commentable.id)
  end

  def destroy
    authorize Comment
    @comment = Comment.find(params[:id])
    if @comment.discard
      flash[:success] = 'Comment has been rejected!'
    else
      raise_exception(@comment)
    end
    redirect_to admin_comments_path
  end
end
