# frozen_string_literal: true

# Controller to register a user with his profile
class Admin::UsersController < ApplicationController
  include Errors

  def new
    authorize User
    @form = RegistrationForm.new
  end

  def create
    authorize User
    @form = RegistrationForm.new(registration_params.merge(user: current_user))
    if @form.save
      flash[:success] = 'User has been successfully created'
      redirect_to root_path
    else
      raise_exception(@form)
      render :new
    end
  end

  def destroy
    authorize User
    @form = User.find(params[:id])
    if @form.discard
      flash[:success] = 'User has been successfully removed'
    else
      raise_exception(@form)
    end
    redirect_to users_path
  end

  private

  def registration_params
    params.require(:registration_form).permit(:email, :password, :first_name, :last_name, :address, :phone, :role)
  end
end
