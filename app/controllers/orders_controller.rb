class OrdersController < ApplicationController
  include Comments
  before_action :authenticate_user!, only: %i[update]
  rescue_from Stripe::CardError, Stripe::InvalidRequestError, with: :catch_exception
  before_action :load_data

  def load_data
    @order = if params[:id] && params[:id] != '0'
               Order.find(params[:id])
             else
               current_order
             end
    @order_lines = @order.order_lines
  end

  def index
    @orders = Order.where(user_id: current_user.id).order(:id)
  end

  def show
    @order.update_attribute(:user_id, current_user.id) if user_signed_in? && current_order.user_id != current_user.id
    authorize @order
    display_comments(@order)
  end

  def update_total
    @order_lines = @order.order_lines
    @order_lines.each_with_index do |ol, i|
      ol.update_attribute(:quantity, filter_params["q_#{i}"])
      ol.save
    end
    if @order.save
      flash[:success] = 'Order was updated'
      redirect_to order_path(@order.id)
    else
      flash[:error] = 'Order was not updated'
      redirect_back(fallback_location: root_path)
    end
  end

  # Buy and finalize order
  def update
    if @order.open? && !@order.new_record?
      BillingCreator.call(charges_params, current_user)
      @order = CartFinalizer.call(@order)
      flash[:success] = 'Order has been successfully finalized'
      redirect_to root_path
    else
      flash[:error] = "You can't buy a cart that isn't open or saved"
      redirect_back(fallback_location: root_path)
    end
  end

  private

  def filter_params
    params[:filter]
  end

  def charges_params
    params.permit(:stripeEmail, :stripeToken, :id)
  end

  def catch_exception(exception)
    flash[:error] = exception.message
    redirect_back(fallback_location: root_path)
  end
end
