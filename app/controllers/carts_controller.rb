class CartsController < ApplicationController
  # Add one line per time
  before_action :load_data
  def load_data
    @order = if current_order.status != 'complete'
               current_order
             else
               Order.new(status: 'open')
             end
  end

  def create
    @order = CartCreator.call(@order, product_params)
    if @order.persisted?
      flash[:success] = 'Order has been successfully created'
      session[:order_id] = @order.id
      redirect_to order_path(@order.id)
    end
  end

  private

  def find_cart
    @order = CartFinder.call(current_user)
  end

  def product_params
    params.require(:post).permit(:product_id, :quantity)
  end
end
