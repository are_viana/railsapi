# frozen_string_literal: true

# Class for OrderLinesController
class OrderLinesController < ApplicationController
  before_action :load_data
  def load_data
    @order = if current_order.status != 'complete'
               current_order
             else
               Order.new(status: 'open')
             end
  end

  def create
    current_product = Product.find(params[:order_line][:product_id])
    @order.save
    @order_line = @order.order_lines.new(order_id: @order.id, product_id: current_product.id, price: current_product.price, quantity: 1)
    if @order_line.save
      @order.save
      flash[:success] = 'Order has been successfully created'
      session[:order_id] = @order.id # Save session var
      current_order # Update current order from application_controller method
      redirect_to order_path(@order.id)
    else
      flash[:error] = @order_line.errors[:product_id]
      redirect_to root_path
    end
  end

  def destroy
    @order_line = OrderLine.find(params[:id])
    if @order_line.destroy
      flash[:success] = 'Order line was succesfully deleted'
      redirect_to order_path(@order.id)
    else
      flash[:error] = 'Order was not deleted'
      redirect_back(fallback_location: root_path)
    end
  end
end
