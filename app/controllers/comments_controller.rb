# frozen_string_literal: true

# Class for CommentsController
class CommentsController < ApplicationController
  before_action :load_commentable

  def new
    @comment = @commentable.comments.new
  end

  def create
    @comment = @commentable.comments.new(params_comment)
    @comment.rati = params_rating[:rating].to_i if @comment.commentable_type == 'Product'
    @comment.status = 'pending' if @comment.commentable_type == 'User'
    if @comment.save
      if @comment.commentable_type == 'User'
        flash[:success] = 'Comment created, wait for approval'
      else
        flash[:success] = 'Comment successfully created'
      end
    else
      flash[:error] = 'Comment was not created'
    end
    redirect_to @commentable
  end

  private

  def params_comment
    params.require(:comment).permit(:description).merge(user_id: current_user.id)
  end

  def params_rating
    params.require(:comment).permit(:rating)
  end

  def load_commentable
    resource, id = request.path.split('/')[1, 2]
    @commentable = resource.singularize.classify.constantize.find(id)
  end
end
