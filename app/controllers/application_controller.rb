# frozen_string_literal: true

# Class for ApplicationController
class ApplicationController < ActionController::Base
  include Pundit

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from ActiveRecord::RecordInvalid, with: :invalid_record
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  rescue_from ActionView::MissingTemplate do |exception|
    logger.error exception.message
    render plain: '404 Not found', status: 404
  end

  protect_from_forgery
  helper_method :current_order

  def current_order
    if session[:order_id].present?
      Order.find(session[:order_id])
    else
      Order.new(status: 'open')
    end
  end

  private

  def user_not_authorized
    flash[:alert] = 'You are not authorized to perform this action'
    redirect_to root_path
  end

  def record_not_found
    flash[:error] = 'Record not found!'
    redirect_to root_path
  end

  # To model errors
  def invalid_record(err)
    flash.now[:error] = err.record.errors.full_messages.join('<br>')
    action = case action_name
             when 'update'
               :edit
             when 'create'
               :new
             else
               action_name
             end
    render action
  end
end
