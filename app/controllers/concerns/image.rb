module Image
  require 'mini_magick'

  def resize(img_path, height, width)
    height.present? && height.to_i.positive? ? height : height = 200
    width.present? && width.to_i.positive? ? width : width = 200
    image = MiniMagick::Image.open(img_path)
    image.resize "#{height.to_i}x#{width.to_i}"
  end
end
