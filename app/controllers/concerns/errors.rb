# frozen_string_literal: true

# Module to raise exception. Used in  product controller
module Errors
  def raise_exception(model)
    if model.errors.present?
      raise ActiveRecord::RecordInvalid.new(model)
    end
  end
end
