# frozen_string_literal: true

# Module to find the commentable subject and its comments
module Comments
  def display_comments_user(model)
    @commentable = model
    @comments = @commentable.comments.approved
    @comment = Comment.new
  end
  def display_comments(model)
    @commentable = model
    @comments = @commentable.comments
    @comment = Comment.new
  end
end