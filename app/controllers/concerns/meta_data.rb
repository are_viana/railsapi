module MetaData
  def meta_data(model)
    {
      records_total: model.count,
      records_per_page: model.page.size,
      current_page: model.page.current_page,
      total_pages: model.page.total_pages,
      next_page: model.page.next_page,
      prev_page: model.page.prev_page
    }
  end
end