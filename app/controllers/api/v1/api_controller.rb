class Api::V1::ApiController < ActionController::API
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  rescue_from ActionController::RoutingError, with: :route_error
  rescue_from ActiveRecord::RecordInvalid, with: :unprocessable_record
  rescue_from ActiveRecord::RecordNotDestroyed, with: :record_not_destroyed
  respond_to :json
  include Pundit

  def unprocessable_record(exception)
    #Api::V1::ValidationErrorsSerializer.name
    errors = ::ValidationErrorsSerializer.new(exception).serialize
    render json: {
      message: 'Validation Failed',
      errors: errors
    }, status: :unprocessable_entity
  end
  # errors: exception.record.errors
  # errors: ValidationErrorsSerializer.new(exception.record).serialize

  def record_not_destroyed(exception)
    render json: {
      message: 'Record not destroyed',
      errors: exception.record.errors
    }, status: :conflict
  end

  def user_not_authorized(exception)
    render json: {
      message: 'Unauthorized action',
      details: "The user #{exception.policy.user.role} does not have permission to perform this #{exception.policy.record} action"
    }, status: :forbidden
  end

  def record_not_found(exception)
    render json: {
      message: 'Record not found',
      resource: exception.model,
      details: "The id = #{exception.id} wasn't found"
    }, status: :not_found
  end

  def route_error
    render json: { error: 'Bad request' }, status: 400
  end

  def current_user
    User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
  end
end
