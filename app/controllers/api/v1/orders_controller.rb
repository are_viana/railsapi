# frozen_string_literal: true

# Class for OrdersController
class Api::V1::OrdersController < Api::V1::ApiController
  before_action :found_order, only: %i[show update]
  def index
    @orders = current_user.orders.order(:id)
    @orders = OrderRepresenter.for_collection.new(@order.includes(order_lines: :product))
    render json: { orders: @orders }, status: :ok
  end

  def show
    authorize @order
    @order = OrderRepresenter.new(@order)
    render json: @order, status: :ok
  end

  def found_order
    @order = Order.find(params[:id])
  end
end
