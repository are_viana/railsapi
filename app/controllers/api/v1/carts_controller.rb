# frozen_string_literal: true

# Class for CartsController
class Api::V1::CartsController < Api::V1::ApiController
  before_action :doorkeeper_authorize!,
                only: %i[destroy finalize discontinue merge]

  before_action :find_cart
  # To show current_order without params[:id]
  def index
    @order = OrderRepresenter.new(@order)
    render json: @order, status: :ok
  end

  # Add one line per time
  def create
    @order = CartCreator.call(@order, product_params)
    @order = OrderRepresenter.new(@order)
    render json: @order, status: :ok
  end

  def destroy
    @order.destroy!
    render json: {
      message: 'The cart was successfully deleted'
    }, status: :gone
  end

  # I make sure that the order_line belongs to order
  def update_line
    @order.order_lines.find(product_params[:line_id])
          .update_attribute(quantity: product_params[:quantity])
    @order = OrderRepresenter.new(@order)
    render json: @order, status: :ok
  end

  def delete_line
    @order.order_lines.destroy!(product_params[:line_id])
    @order = OrderRepresenter.new(@order)
    render json: @order, status: :ok
  end

  def merge(guest_cart)
    @order = CartMerger.call(guest_cart)
    @order = OrderRepresenter.new(@order)
    render json: @order, status: :ok
  end

  # Buy and finalize order
  def finalize
    if @order.open? && !@order.new_record?
      @order = CartFinalizer.call(@order)
      @order = OrderRepresenter.new(@order)
      render json: @order, status: :ok
    else
      render json: {
        message: "You can't buy a cart that isn't open or saved"
      }, status: :conflict
    end
  end

  def discontinue
    if !@order.complete? && !@order.new_record? && @order.order_lines.present?
      @order.canceled! if params[:canceled]
      @order.abandoned! if params[:abandoned]
      @order = OrderRepresenter.new(@order)
      render json: @order, status: :ok
    else
      render json: {
        message: 'No cart to discontinue'
      }, status: :conflict
    end
  end

  private

  def find_cart
    @order = CartFinder.call(current_user)
  end

  def product_params
    params.require(:post).permit(:product_id, :quantity)
  end

  def lines_params
    params.require(:lines).permit(:line_id, :quantity)
  end
end
