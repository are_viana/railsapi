# frozen_string_literal: true

# Class for ProductsController
class Api::V1::ProductsController < Api::V1::ApiController
  include MetaData
  before_action :doorkeeper_authorize!,
                only: %i[create update destroy discarded like dislike]

  before_action :found_product, only: %i[like dislike]

  def index
    @products = policy_scope(Product).page params[:page] # Policy_show products
    meta = meta_data(@products)
    @products = ProductSearcher.call(@products, search_params).page params[:page]
    @products = ProductRepresenter.for_collection.new(@products)
    render json: { products: @products, pagination_data: meta }, status: :ok
  end

  def show
    @products = policy_scope(Product)
    @product = @products.find(params[:id])
    @product = ProductRepresenter.new(@product)
    render json: @product, status: :ok
  end

  def create
    authorize Product
    @product = ProductCreator.call(product_params)
    @product = ProductRepresenter.new(@product)
    render json: @product, status: :ok
  end

  def update
    authorize Product
    @product = ProductUpdater.call(product_params, params[:id], current_user)
    @product = ProductRepresenter.new(@product)
    render json: @product, status: :ok
  end

  def destroy
    authorize Product
    @product = ProductDestroyer.call(params[:id])
    @product = ProductRepresenter.new(@product)
    render json: @product, status: :ok
  end

  def discarded
    authorize Product
    @products = Product.discarded
    @products = ProductRepresenter.for_collection.new(@products)
    render json: @products, status: :ok
  end

  def like
    @product.liked_by current_user
    @product = ProductRepresenter.new(@product)
    render json: @product, status: :ok
  end

  def dislike
    @product.disliked_by current_user
    @product = ProductRepresenter.new(@product)
    render json: @product, status: :ok
  end

  private

  def found_product
    @product = Product.active.find(params[:product_id])
  end

  def product_params
    params.require(:product).permit(:name, :description, :price, :stock, :status, tag_ids: [], image: {})
  end

  def search_params
    params.require(:search).permit(:name, :popularity, tag_ids: [])
  end
end
