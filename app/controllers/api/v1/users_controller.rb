# frozen_string_literal: true

# Controller to show a user with his profile
class Api::V1::UsersController < Api::V1::ApiController
  before_action :doorkeeper_authorize!,
                only: %i[index show]
  def index
    @users = policy_scope(User)
    @users = UserRepresenter.for_collection.new(@users)
    render json: { users: @users }, status: :ok
  end

  def show
    @users = policy_scope(User)
    @user = @users.find(params[:id])
    @user = UserRepresenter.new(@user)
    render json: @user, status: :ok
  end
end
