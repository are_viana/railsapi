# frozen_string_literal: true

# Controller to handle password operations of an user
class Api::V1::PasswordController < Api::V1::ApiController
  def create
    User.find_by(user_email)
    if user_email
      user.send_reset_password_instructions if user.present?
      render json: { success: 'An email with instuctions has been sent' }, status: :ok
    else
      render json: { error: "Email can't be blank" }, status: 409
    end
  end

  def update
    if params[:reset_password_token].nil?
      render json: {error: 'You need a valid token to perfom this action'}
    else
      token = params[:reset_password_token]
      user = User.with_reset_password_token(token)
      if user.present? && user.reset_password_period_valid?
        if user.reset_password(passwords[:password], passwords[:password_confirmation])
          render json: { success: 'The password was successfully reset' }, status: :ok
        else
          render json: { error: user.errors }, status: :unprocessable_entity
        end
      end
    end
  end

  private

  def user_email
    params.require(:user).permit(:email)
  end

  def passwords
    params.require(:user).permit(:password, :password_confirmation)
  end
end
