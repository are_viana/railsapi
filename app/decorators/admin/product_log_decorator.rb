class Admin::ProductLogDecorator < Draper::Decorator
  delegate_all

  def date_created_at
    created_at.strftime("%A, %B %e")
  end
end
