class OrderRepresenter < Representable::Decorator
  include Representable::JSON
  property :id
  property :user_id
  property :status
  property :total
  property :finished_at

  collection :order_lines do
    property :id
    property :product_id
    property :product_name
    property :price
    property :quantity
    property :total
  end
  collection :comments do
    property :user_full_name
    property :user_email
    property :description
    property :rati, as: :rating
  end
end
