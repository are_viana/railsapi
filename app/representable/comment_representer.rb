class CommentRepresenter < Representable::Decorator
  include Representable::JSON
  property :id
  property :commentable_type
  property :commentable_id
  property :user_full_name
  property :user_email
  property :description
  property :rati, as: :rating
  property :status
  # Como le mando la tabla polimorfica?
end
