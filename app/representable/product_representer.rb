class ProductRepresenter < Representable::Decorator
  include Representable::JSON
  property :id
  property :sku
  property :name
  property :description
  property :price
  property :stock
  property :status
  property :cached_votes_up, as: :likes
  property :cached_votes_down, as: :dislikes
  property :discarded_at
  property :image_on_disk

  collection :tags do
    property :id
    property :name
  end
  collection :comments do
    property :user_full_name
    property :user_email
    property :description
    property :rati, as: :rating
  end
end
