class UserRepresenter < Representable::Decorator
  include Representable::JSON

  property :id
  property :full_name
  property :email
  property :role

  collection :comments do
    property :user_full_name
    property :user_email
    property :description
    property :rati, as: :rating
  end
end