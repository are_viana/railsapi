# README

This README would normally document whatever steps are necessary to get the
application up and running.

* Ruby version: 2.3.3
* Rails version: 5.2.3
* Data-base engine recommended: PostgreSQL

## HEROKU
* https://protected-savannah-18037.herokuapp.com

## ABOUT THIS PROJECT
In this Snacks Store web app you can:
* With admin credentials:
  - Make a CRUD of products.
  - View the product price change log
* With a logout user:
  - Open a shopping cart
* With a login user:
  - Comment on a product
  - Make a rating on a product
  - View your order history
  - Complete a purchase
* Preview mailer
  - http://localhost:3000/rails/mailers/user_mailer

* EXTRA:
  - If, after running the seeds, you don't see the pagination function, you probably don't have enough active products, to solve it, run the following command in the rails console:
  ``` Product.update_all(status: 'active') ```
* Services (external)
  - Random images from: http://lorempixel.com

### System dependencies
  * Gems included
    - Acts as votable [gem 'acts_as_votable'](https://github.com/ryanto/acts_as_votable)
    - Pagination [gem 'kaminari'](https://github.com/kaminari/kaminari)
    - Soft delete: [gem 'discard'](https://github.com/jhawthorn/discard)
    - User authentication [gem 'devise'](https://github.com/plataformatec/devise)
    - Environment Variables [gem 'dotenv-rails'](https://github.com/bkeepers/dotenv)
    - Generate faker data [gem 'faker'](https://github.com/stympy/faker)
    - Check best practices [gem 'rails_best_practices'](https://github.com/flyerhzm/rails_best_practices)
    - Applied to debug and test code:
      - [gem 'byebug'](https://github.com/deivid-rodriguez/byebug)
      - [gem 'factory_bot_rails']
      - [gem 'rspec-rails'](https://github.com/rspec/rspec-rails)
      - [gem 'rails-controller-testing'](https://github.com/rails/rails-controller-testing)
      - [gem 'shoulda-matchers'](https://github.com/thoughtbot/shoulda-matchers)
    
  * Install gemfile dependencies
    ```bash
    bundle install 
    ```

### Database initialization
  ```bash
  rails db:create
  rails db:migrate
  rails db:seed
  ```

### Deployment instructions
  * Default port: 3000
  * Start up server:
    ```bash
    rails s
    ```

* Default user tester:
  - email: 'admin@example.com',  password: '123456',  role: 'Admin'
  - email: 'customer@example.com',  password: '123456',  role: 'Customer'

### How to run the test suite
  * Create database in test environment:
    ```bash
    rails db:create RAILS_ENV=test
    rails db:migrate RAILS_ENV=test
    rails db:seed RAILS_ENV=test
    ```
  * Run the tests: 
    ```bash
    bundle exec rspec
    ```
  * To inspect code, in your root directory of a Rails app run:
    - With rails best practices:
      ```bash
      rails_best_practices . -c config/rails_best_practices.yml
      ```
    - With rubocop:
      ```bash
      gem install rubocop
      rubocop
      ```
### Enviroment variables
```
DB_USERNAME=
REDIS_URL=
PASSWORD=
DEV_DATABASE=
PROD_DATABASE=
TEST_DATABASE=
DEV_PORT=
PUBLISHABLE_KEY=
SECRET_KEY=
RACK_ENV=
PORT=
```
----------