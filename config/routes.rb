# frozen_string_literal: true

# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => "/sidekiq"
  use_doorkeeper do
    skip_controllers :authorizations, :applications,
                     :authorized_applications
  end
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      get 'products/discarded', to: 'products#discarded'
      resources :products, except: %i[new edit] do
        put 'like', to: 'products#like'
        put 'dislike', to: 'products#dislike'
      end
      resources :orders, only: %i[index show]
      resources :carts, only: [:index], path: 'cart'
      resource :carts, except: %i[new edit show update] do
        put 'finalize', to: 'carts#finalize'
        put 'merge', to: 'carts#merge'
        patch 'discontinue', to: 'carts#discontinue'
        put 'update_line', to: 'carts#update_line'
        put 'delete_line', to: 'carts#delete_line'
      end
      resources :users, only: %i[index show]
      post 'users/password', to: 'passwords#create'
      put 'users/password', to: 'passwords#update'
    end
  end
  root to: 'products#index'
  resources :billing, only: %i[new create]
  resources :carts, only: %i[create update]
  resources :order_lines, only: %i[create destroy]
  resources :orders do
    put 'update_total', to: 'orders#update_total'
    resources :comments, only: %i[new create]
  end
  resources :products, only: %i[index show] do
    put 'like', to: 'products#like'
    put 'dislike', to: 'products#dislike'
    resources :comments, only: %i[new create]
  end
  devise_for :users
  resources :users, only: %i[index show] do
    resources :comments, only: %i[new create]
  end
  namespace :admin do
    get 'products/list', to: 'products#list'
    resources :products
    resources :product_logs, only: [:index]
    resources :users, only: %i[new create destroy]
    resources :comments, only: %i[index destroy update]
  end
end
