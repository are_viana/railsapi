# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  def stock_email
    UserMailer.stock_email(User.first, Product.active.first)
  end
end
