require 'rails_helper'

RSpec.describe ProductsController, type: :controller do
  describe "GET index" do
    it "assigns @products" do
      products = Product.active.page
      get :index
      expect(assigns(:products)).to eq(products)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end
end
