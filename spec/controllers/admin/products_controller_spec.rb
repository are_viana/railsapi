# frozen_string_literal: true

# Something

require 'rails_helper'

RSpec.describe Admin::ProductsController, type: :controller do
  let(:admin) { User.find(1) }
  let(:current_product) { Product.find(1) }

  describe "POST create" do
    it 'creates a product' do
      sign_in admin
      params = attributes_for(:product)
      expect { post :create, params: { product: params } }.to change(Product, :count).by(1)
    end
  end

  describe "PUT update" do
    it 'update product attributes' do
      sign_in admin
      put :update, params: { id: current_product.id, product: { name: 'Holi'} }
      expect(current_product.reload.name).to eq('Holi')
    end
  end

  describe "DELETE destroy" do
    it 'soft delete product' do
      sign_in admin
      delete :destroy, params: { id: current_product.id }
      expect(current_product.reload.status).to eq('inactive')
      expect(current_product.reload.discarded_at).to be_truthy
    end
  end
end
