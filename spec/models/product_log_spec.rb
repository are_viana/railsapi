require 'rails_helper'

RSpec.describe ProductLog, type: :model do
  context 'associations' do
    it { should belong_to(:product) }
    it { should belong_to(:user).with_foreign_key('admin_id') }
  end

  context 'validations' do
    it { should validate_presence_of(:admin_id) }
    it { should validate_presence_of(:product_id) }
    it { should validate_presence_of(:price_before) }
    it { should validate_presence_of(:price_after) }
    it { should validate_numericality_of(:price_before) }
    it { should validate_numericality_of(:price_after) }
  end
end
