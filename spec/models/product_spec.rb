# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Product, type: :model do
  let(:sku) { SecureRandom.uuid }
  subject { Product.new(sku: sku) }

  context 'associations' do
    it { should have_many(:order_lines) }
    it { should have_many(:orders).through(:order_lines) }
    it { should have_many(:product_tags) }
    it { should have_many(:tags).through(:product_tags) }
    it { should have_many(:product_logs) }
    it { should have_many(:comments) }
  end

  context 'validations' do
    it { should validate_presence_of(:status) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:stock) }
    it { should validate_uniqueness_of(:sku) }
    it { should validate_uniqueness_of(:name) }
    it { should validate_numericality_of(:price) }
    it { should validate_numericality_of(:stock) }
  end

  context 'enum_status_validation' do
    it { should define_enum_for(:status) }
  end

  context 'callback' do
    it 'SKU generated automatically' do
      expect(subject.sku).to eq(sku)
    end
  end
end
