# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrderLine, type: :model do
  subject { OrderLine.first }
  let(:current_order) { subject.order }
  let(:current_product) { subject.product }
  let(:total) { subject.quantity * current_product.price }

  context 'associations' do
    it { should belong_to(:order) }
    it { should belong_to(:product) }
  end

  context 'validations' do
    it { should validate_numericality_of(:order_id) }
    it { should validate_numericality_of(:product_id) }
    it { should validate_numericality_of(:quantity) }
    it { should validate_numericality_of(:price) }
  end

  context 'callback' do
    it 'calculate order line total' do
      expect(subject.total).to eq(total)
    end
    context 'verify stock' do
      it 'the stock is enough' do
        current_product.stock = 25
        expect(subject.quantity).to be <= current_product.stock
      end
    end
    it 'verify price' do
      expect(subject.price).to eq(current_product.price)
    end
    context 'validate order lines' do
      describe 'An order cannot have 2 order_lines with the same products' do
        it 'unique product per line' do
          # No puedo validar que no exista
        end
      end
    end
    context 'before destroy' do
      it 'update total order' do
        @total_new = current_order.total - total
        subject.run_callbacks :destroy
        expect(current_order.total.to_f).to be(@total_new.to_f)
      end
    end
  end
end
