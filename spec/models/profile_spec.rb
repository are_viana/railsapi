require 'rails_helper'

RSpec.describe Profile, type: :model do
  context 'associations' do
    it { should belong_to(:user) }
  end
  context 'validations' do
    it { should validate_presence_of(:user_id) }
    it { should validate_numericality_of(:user_id) }
  end
end
