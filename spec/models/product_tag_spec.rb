# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProductTag, type: :model do
  context 'associations' do
    it { should belong_to(:product) }
    it { should belong_to(:tag) }
  end

  context 'validations' do
    it { should validate_presence_of(:product_id) }
    it { should validate_presence_of(:tag_id) }
    it { should validate_uniqueness_of(:product_id).scoped_to(:tag_id) }
  end
end
