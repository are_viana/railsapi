# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Order, type: :model do
  subject { Order.new(status: 'Open') }
  let(:order) { Order.first }
  let(:order_lines_sum) { order.order_lines.sum(:total) }

  context 'associations' do
    it { should belong_to(:user).optional }
    it { should have_many(:products).through(:order_lines) }
    it { should have_many(:order_lines) }
    it { should have_many(:comments) }
  end

  context 'validations' do
    it { should validate_presence_of(:status) }
    it { should validate_numericality_of(:total) }
  end

  context 'callback' do
    it 'update_total_order' do
      expect(order.total).to eq(order_lines_sum)
    end
    it 'set status order' do
      expect(subject.status).to eq('Open')
    end
  end
end
