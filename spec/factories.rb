FactoryBot.define do
  factory :user do
    id { 50 }
    email {'admin@admin@gmail.com'}
    password {'123456'}
    role {'admin'}
  end
  factory :product do
    id { 50 }
    sku { SecureRandom.uuid }
    name { 'Avocado' }
    description { 'The avocado is amazing' }
    price { 1.25 }
    stock { 25 }
    status { 'active' }
  end
  factory :order do
    id { 50 }
    user_id { 50 }
    finished_at {'2019-07-26'}
    total { 10 }
  end
  factory :order_line do
    id { 50 }
    order_id { 50 }
    product_id { 50 }
    price { 5 }
    quantity { 2 }
    total { 10 }
  end
end
