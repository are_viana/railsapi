FROM ruby:2.6.3
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client && gem install bundler
RUN mkdir /railsapi
WORKDIR /railsapi
COPY Gemfile /railsapi/Gemfile
COPY Gemfile.lock /railsapi/Gemfile.lock
RUN bundle install
COPY . /railsapi
RUN chmod +x ./docker-entrypoint.sh
EXPOSE 3000
